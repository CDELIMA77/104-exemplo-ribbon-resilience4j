package br.com.mastertech.pessoa.pessoa.model.dto;

import br.com.mastertech.pessoa.pessoa.client.CarroDTO;
import com.fasterxml.jackson.annotation.JsonProperty;

public class GetPessoaResponse {

    private Long id;

    private String name;

    @JsonProperty("carro")
    private CarroDTO carroDTO;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CarroDTO getCarroDTO() {
        return carroDTO;
    }

    public void setCarroDTO(CarroDTO carroDTO) {
        this.carroDTO = carroDTO;
    }
}
